package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;


    @PostMapping("/api/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest createProductRequest) {
        Product product = productRepository.saveAndFlush(new Product(createProductRequest.getName(), createProductRequest.getPrice(), createProductRequest.getUnit()));
        return ResponseEntity.status(HttpStatus.CREATED).header("location", "http://localhost/api/products/1").body(null);

    }

    @GetMapping("/api/products/{id}")
    public ResponseEntity<GetProductResponse> getProduct(@PathVariable Long id) {
        Optional<Product> productOptional = productRepository.findById(id);
        GetProductResponse productResponse = new GetProductResponse(productOptional.get());
        return ResponseEntity.status(HttpStatus.OK).header("location", "http://localhost/api/products/1").body(productResponse);

    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity handleException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
    }


}
